package models

import (
	"mt-go/db"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/golang/protobuf/ptypes/timestamp"
)

type Account struct {
	ID                int                 `json:"id"`
	Name              string              `json:"name"`
	Email             string              `json:"email`
	Email_verified_at timestamp.Timestamp `json:"email_verified_at"`
	Password          string              `json:"password`
	Remember_token    string              `json:"remember_token"`
	Slug              string              `json:"slug"`
	Description       string              `json:"description"`
	Status            string              `json:"status"`
	Created_at        timestamp.Timestamp `json:"created_at"`
	Updated_at        timestamp.Timestamp `json:"updated_at"`
}

func FetchAllUSer() (Response, error) {
	var obj Account
	var arrobj []Account
	var res Response

	con := db.CreateCon()

	sqlStatement := "SELECT * FROM users"

	rows, err := con.Query(sqlStatement)
	defer rows.Close()

	if err != nil {
		return res, err
	}

	for rows.Next() {
		err = rows.Scan(
			&obj.ID, &obj.Name, &obj.Email, &obj.Password,
			&obj.Created_at, &obj.Updated_at, &obj.Email_verified_at,
			&obj.Remember_token,
		)
		if err != nil {
			return res, err
		}

		arrobj = append(arrobj, obj)
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = arrobj

	return res, nil
}

func CreateUser(name string, email string, password string) (Response, error) {
	var res Response

	v := validator.New()

	user := Account{
		Name:     name,
		Email:    email,
		Password: password,
	}

	err := v.Struct(user)
	if err != nil {
		return res, err
	}

	con := db.CreateCon()

	sqlStatement := "INSERT users (name, email, password) VALUES (?, ?, ?)"

	stmt, err := con.Prepare(sqlStatement)
	if err != nil {
		return res, err
	}

	result, err := stmt.Exec(name, email, password)
	if err != nil {
		return res, err
	}

	lastInsertedId, err := result.LastInsertId()
	if err != nil {
		return res, err
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = map[string]int64{
		"last_inserted_id": lastInsertedId,
	}

	return res, nil
}

func UpdateUser(id int, name string, email string, password string) (Response, error) {
	var res Response

	con := db.CreateCon()

	sqlStatement := "UPDATE user SET name = ?, email = ?, password = ? WHERE id = ?"

	stmt, err := con.Prepare(sqlStatement)
	if err != nil {
		return res, err
	}

	result, err := stmt.Exec(name, email, password)
	if err != nil {
		return res, err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return res, err
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = map[string]int64{
		"rows_affected": rowsAffected,
	}

	return res, nil
}

func DeleteUser(id int) (Response, error) {
	var res Response

	con := db.CreateCon()

	sqlStatement := "DELETE FROM user WHERE id = ?"

	stmt, err := con.Prepare(sqlStatement)
	if err != nil {
		return res, err
	}

	result, err := stmt.Exec(id)
	if err != nil {
		return res, err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return res, err
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = map[string]int64{
		"rows_affected": rowsAffected,
	}

	return res, nil
}
