package routes

import (
	"net/http"

	"mt-go/controllers"

	"github.com/labstack/echo/v4"
)

func Init() *echo.Echo {
	e := echo.New()

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, this is echo!")
	})

	e.GET("/users", controllers.FetchAllUSer)
	e.POST("/user", controllers.CreateUser)
	e.PUT("/user", controllers.CreateUser)
	e.DELETE("/user", controllers.CreateUser)

	return e
}
